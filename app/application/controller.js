import Controller from '@ember/controller';
import {get} from '@ember/object';

class LEDStripe {
  constructor(len) {
    this.len = len;
    this.arr = new Uint8Array(len * 3);
  }

  fill(...args) {
    for(let i = 0; i < this.arr.length; i+= args.length) {
      this.arr.set(args, i);
    }
    return this;
  }

  write() {
    fetch('http://10.0.0.3', {
      method: 'POST',
      body: this.arr,
    });
    return this;
  }
}

export default Controller.extend({
  stripe: new LEDStripe(42),
  actions: {
    same(a,b,c) {
      get(this, 'stripe').fill(a,b,c).write();
    }
  }
});
